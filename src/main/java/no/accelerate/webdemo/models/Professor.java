package no.accelerate.webdemo.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import no.accelerate.webdemo.models.Student;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToMany(mappedBy = "professor")
    private Set<Student> students;
    @OneToMany(mappedBy = "professor")
    private Set<Subject> subjects;

    @JsonGetter("students")
    public List<Integer> jsonGetStudents() {
        if(students == null)
            return null;
        return students.stream().map(s -> s.getId()).collect(Collectors.toList());
    }

    @JsonGetter("subjects")
    public List<Integer> jsonGetSubjects() {
        if(subjects == null)
            return null;
        return subjects.stream().map(s -> s.getId()).collect(Collectors.toList());
    }
}
