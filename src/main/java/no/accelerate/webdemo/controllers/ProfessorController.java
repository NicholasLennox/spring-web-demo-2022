package no.accelerate.webdemo.controllers;

import no.accelerate.webdemo.models.Professor;
import no.accelerate.webdemo.services.professor.ProfessorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/professors")
public class ProfessorController {

    private final ProfessorService professorService;

    public ProfessorController(ProfessorService professorService) {
        this.professorService = professorService;
    }

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(professorService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable int id) {
        return ResponseEntity.ok(professorService.findById(id));
    }

    @PostMapping
    public ResponseEntity add(@RequestBody Professor prof) {
        Professor newProf = professorService.add(prof);
        URI uri = URI.create("professors/" + newProf.getId());
        return ResponseEntity.created(uri).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Professor prof, @PathVariable int id) {
        if(prof.getId() != id)
            return ResponseEntity.badRequest().build();
        professorService.update(prof);
        return ResponseEntity.noContent().build();
    }
}
