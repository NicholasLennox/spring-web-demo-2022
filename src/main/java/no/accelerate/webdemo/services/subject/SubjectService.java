package no.accelerate.webdemo.services.subject;

import no.accelerate.webdemo.models.Subject;
import no.accelerate.webdemo.services.CrudService;

public interface SubjectService extends CrudService<Subject, Integer> {
}
