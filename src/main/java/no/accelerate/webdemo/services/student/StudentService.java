package no.accelerate.webdemo.services.student;

import no.accelerate.webdemo.models.Student;
import no.accelerate.webdemo.services.CrudService;

public interface StudentService extends CrudService<Student, Integer> {
}
