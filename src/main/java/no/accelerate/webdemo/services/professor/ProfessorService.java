package no.accelerate.webdemo.services.professor;

import no.accelerate.webdemo.models.Professor;
import no.accelerate.webdemo.services.CrudService;

public interface ProfessorService extends CrudService<Professor, Integer> {
    boolean exists(int id);
}
