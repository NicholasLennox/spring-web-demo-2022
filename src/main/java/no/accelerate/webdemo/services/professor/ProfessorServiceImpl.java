package no.accelerate.webdemo.services.professor;

import no.accelerate.webdemo.services.exceptions.ProfessorNotFoundException;
import no.accelerate.webdemo.models.Professor;
import no.accelerate.webdemo.repositories.ProfessorRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ProfessorServiceImpl implements ProfessorService {

    private final ProfessorRepository professorRepository;

    public ProfessorServiceImpl(ProfessorRepository professorRepository) {
        this.professorRepository = professorRepository;
    }

    @Override
    public Professor findById(Integer id) {
        return professorRepository.findById(id)
                .orElseThrow(() -> new ProfessorNotFoundException(id));
    }

    @Override
    public Collection<Professor> findAll() {
        return professorRepository.findAll();
    }

    @Override
    public Professor add(Professor entity) {
        return professorRepository.save(entity);
    }

    @Override
    public Professor update(Professor entity) {
        return professorRepository.save(entity);
    }

    @Override
    public void deleteById(Integer id) {

    }

    @Override
    public boolean exists(int id) {
        return professorRepository.existsById(id);
    }
}
