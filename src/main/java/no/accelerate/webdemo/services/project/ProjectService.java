package no.accelerate.webdemo.services.project;

import no.accelerate.webdemo.models.Project;
import no.accelerate.webdemo.services.CrudService;

public interface ProjectService extends CrudService<Project, Integer> {
}
